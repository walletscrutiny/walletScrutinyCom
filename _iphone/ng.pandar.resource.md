---
wsId: pandar
title: Pandar App
altTitle: 
authors:
- danny
appId: ng.pandar.resource
appCountry: us
idd: '1563046132'
released: 2021-04-20
updated: 2025-02-26
version: 1.8.2
stars: 4.5
reviews: 3030
website: 
repository: 
issue: 
icon: ng.pandar.resource.jpg
bugbounty: 
meta: ok
verdict: wip
appHashes: 
date: 2024-09-02
signer: 
reviewArchive: 
twitter: PandarNG
social: 
features: 
developerName: PANDAR RESOURCES LIMITED

---

{% include copyFromAndroid.html %}
