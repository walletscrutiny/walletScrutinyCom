---
wsId: KangaWallet
title: Kanga Wallet
altTitle: 
authors:
- danny
appId: kanga.mobile
appCountry: jp
idd: '1471634153'
released: 2020-02-13
updated: 2025-02-21
version: 1.21.4
stars: 2.5
reviews: 6
website: http://kanga.exchange
repository: 
issue: 
icon: kanga.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: KangaExchange
social:
- https://www.linkedin.com/company/kangaexchange/
- https://t.me/KangaExchange
- https://www.youtube.com/channel/UCgB3tPtXgu4f3cIxqZlrB8Q
- https://www.facebook.com/kanga.exchange/
features: 
developerName: Grupa IT Sp. z o.o.

---

{% include copyFromAndroid.html %}