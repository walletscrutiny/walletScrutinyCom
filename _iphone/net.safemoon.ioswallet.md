---
wsId: safemoon
title: VGX Wallet
altTitle: 
authors:
- danny
appId: net.safemoon.ioswallet
appCountry: us
idd: '1579735495'
released: 2021-10-06
updated: 2025-03-07
version: '4.1'
stars: 4.8
reviews: 14650
website: https://vgxfoundation.com/
repository: 
issue: 
icon: net.safemoon.ioswallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
appHashes: 
date: 2025-02-26
signer: 
reviewArchive: 
twitter: safemoon
social: 
features: 
developerName: VGX Foundation

---

{% include copyFromAndroid.html %}