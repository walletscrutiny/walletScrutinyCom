---
wsId: apexProTrade
title: 'ApeX Protocol: Trade Crypto'
altTitle: 
authors:
- danny
appId: com.pro.apex
appCountry: us
idd: '1645456064'
released: 2022-09-27
updated: 2025-02-26
version: 3.9.0
stars: 3.9
reviews: 58
website: 
repository: 
issue: 
icon: com.pro.apex.jpg
bugbounty: 
meta: ok
verdict: nowallet
appHashes: 
date: 2023-07-02
signer: 
reviewArchive: 
twitter: OfficialApeXdex
social:
- https://apex.exchange
- https://apexdex.medium.com
- https://discord.com/invite/366Puqavwx
- https://t.me/ApeXdex
features: 
developerName: APEX DAO LLC

---

{% include copyFromAndroid.html %}
