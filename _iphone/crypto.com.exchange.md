---
wsId: cryptocomexchange
title: Crypto.com Exchange
altTitle: 
authors:
- leo
- danny
appId: crypto.com.exchange
appCountry: nz
idd: 1569309855
released: 2021-06-15
updated: 2025-03-05
version: 2.57.0
stars: 4.8
reviews: 102
website: https://crypto.com/exchange
repository: 
issue: 
icon: crypto.com.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-10-24
signer: 
reviewArchive: 
twitter: cryptocom
social:
- https://www.linkedin.com/company/cryptocom
- https://www.facebook.com/CryptoComOfficial
- https://www.reddit.com/r/Crypto_com
features: 
developerName: Foris DAX Limited

---

{% include copyFromAndroid.html %}