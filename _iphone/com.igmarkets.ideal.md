---
wsId: igTradingPlatform
title: 'IG: Trade Stocks, Forex & More'
altTitle: 
authors:
- danny
appId: com.igmarkets.ideal
appCountry: sg
idd: '406492428'
released: 2011-01-24
updated: 2025-03-10
version: 10.2460.0
stars: 4.5
reviews: 2737
website: https://www.ig.com/uk/trading-platforms/trading-apps
repository: 
issue: 
icon: com.igmarkets.ideal.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
appHashes: 
date: 2022-06-23
signer: 
reviewArchive: 
twitter: IGInternationa1
social:
- https://www.linkedin.com/company/ig-international-
- https://www.facebook.com/IGInternational2
- https://www.youtube.com/channel/UCZj-ae-S_X-mocAH3xQnpUw
features: 
developerName: IG Group

---

{% include copyFromAndroid.html %}
