---
wsId: blackcatcard
title: Blackсatсard
altTitle: 
authors:
- danny
appId: com.papaya.blackcatcard
appCountry: lv
idd: 1449352913
released: 2019-03-07
updated: 2025-02-17
version: 1.2.67
stars: 0
reviews: 0
website: https://blackcatcard.com
repository: 
issue: 
icon: com.papaya.blackcatcard.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Papaya Ltd

---

{% include copyFromAndroid.html %}