---
wsId: Bitnob
title: Bitnob
altTitle: 
authors:
- danny
appId: com.app.bitnob
appCountry: us
idd: '1513951003'
released: 2020-05-29
updated: 2025-03-06
version: 1.189.1
stars: 4.3
reviews: 77
website: https://bitnob.com
repository: 
issue: 
icon: com.app.bitnob.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: Bitnob_official
social:
- https://www.linkedin.com/company/bitnob
- https://www.facebook.com/bitnob
features:
- ln
developerName: Bitnob Technologies

---

**Update 2022-01-15**: This app is not on the Store anymore.

{% include copyFromAndroid.html %}

