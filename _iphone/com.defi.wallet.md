---
wsId: cryptoComDefi
title: Crypto.com Onchain
altTitle: 
authors:
- leo
appId: com.defi.wallet
appCountry: 
idd: 1512048310
released: 2020-05-20
updated: 2025-03-10
version: 2.09.0
stars: 4.7
reviews: 12210
website: https://crypto.com/onchain
repository: 
issue: 
icon: com.defi.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2021-10-24
signer: 
reviewArchive: 
twitter: cryptocom
social:
- https://www.linkedin.com/company/cryptocom
- https://www.facebook.com/CryptoComOfficial
- https://www.reddit.com/r/Crypto_com
features: 
developerName: Growth Labs

---

{% include copyFromAndroid.html %}
