---
wsId: croptyWallet
title: Crypto wallet – Bitcoin & USDT
altTitle: 
authors:
- danny
appId: com.cropty.app
appCountry: us
idd: '1624901793'
released: 2022-08-04
updated: 2025-03-09
version: 1.6.3
stars: 4.8
reviews: 133
website: https://cropty.io/
repository: 
issue: 
icon: com.cropty.app.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-07-30
signer: 
reviewArchive: 
twitter: cropty_app
social:
- https://www.youtube.com/@croptytv
features: 
developerName: Coinscatch

---

{% include copyFromAndroid.html %}