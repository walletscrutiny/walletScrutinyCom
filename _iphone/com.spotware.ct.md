---
wsId: cTrader
title: cTrader
altTitle: 
authors:
- danny
appId: com.spotware.ct
appCountry: my
idd: '767428811'
released: 2013-12-05
updated: 2025-03-06
version: 5.2.110
stars: 4.7
reviews: 520
website: https://ctrader.com/forum
repository: 
issue: 
icon: com.spotware.ct.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
appHashes: 
date: 2023-07-01
signer: 
reviewArchive: 
twitter: cTrader
social:
- https://www.linkedin.com/company/ctrader
- https://www.youtube.com/spotware
- https://t.me/cTrader_Official
features: 
developerName: Spotware

---

{% include copyFromAndroid.html %}
