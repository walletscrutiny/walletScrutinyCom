---
wsId: conglex
title: Conglex
altTitle: 
authors:
- danny
appId: com.conglex.mobile
appCountry: us
idd: '1626252670'
released: 2022-06-03
updated: 2023-03-08
version: 1.3.9
stars: 0
reviews: 0
website: https://conglex.com/
repository: 
issue: 
icon: com.conglex.mobile.jpg
bugbounty: 
meta: obsolete
verdict: custodial
appHashes: 
date: 2025-02-26
signer: 
reviewArchive: 
twitter: conglexglobal
social:
- https://www.linkedin.com/company/conglex-nigeria/
features: 
developerName: Conglex Limited

---

{% include copyFromAndroid.html %}

