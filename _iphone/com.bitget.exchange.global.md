---
wsId: Bitget
title: Bitget- Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.bitget.exchange.global
appCountry: ua
idd: 1442778704
released: 2018-11-29
updated: 2025-03-04
version: 2.51.1
stars: 4.5
reviews: 1692
website: https://www.bitget.com/en
repository: 
issue: 
icon: com.bitget.exchange.global.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-09-17
signer: 
reviewArchive: 
twitter: bitgetglobal
social:
- https://www.linkedin.com/company/bitget
- https://www.facebook.com/BitgetGlobal
features: 
developerName: BG LIMITED

---

 {% include copyFromAndroid.html %}
