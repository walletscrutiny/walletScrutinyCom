---
wsId: neteller
title: NETELLER - Money Transfer
altTitle: 
authors:
- danny
appId: com.skrill.NETELLER
appCountry: gb
idd: '1095647938'
released: 2016-05-02
updated: 2025-03-05
version: 3.155.0
stars: 4.5
reviews: 833
website: 
repository: 
issue: 
icon: com.skrill.NETELLER.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-09-07
signer: 
reviewArchive: 
twitter: neteller
social: 
features: 
developerName: Skrill Ltd.

---

{% include copyFromAndroid.html %}