---
wsId: Savl
title: 'Unity Wallet: Crypto & Bitcoin'
altTitle: 
authors:
- danny
appId: com.savl.savlapp
appCountry: ru
idd: 1369912925
released: 2018-04-22
updated: 2025-03-07
version: 8.2.2
stars: 4.5
reviews: 258
website: https://unitywallet.com
repository: 
issue: 
icon: com.savl.savlapp.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2021-09-11
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/savl.official
features: 
developerName: Savl GmbH

---

{% include copyFromAndroid.html %}
