---
wsId: giottus
title: Giottus
altTitle: 
authors:
- danny
appId: com.giottus.app
appCountry: in
idd: 1537068185
released: 2021-01-27
updated: 2025-02-14
version: 3.0.31
stars: 4.4
reviews: 1033
website: 
repository: 
issue: 
icon: com.giottus.app.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: giottus
social:
- https://www.linkedin.com/company/giottus
- https://www.facebook.com/Giottus
features: 
developerName: giottus

---

{% include copyFromAndroid.html %}
