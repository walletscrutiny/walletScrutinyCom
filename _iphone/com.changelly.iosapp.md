---
wsId: changelly
title: Changelly Exchange・Buy Crypto
altTitle: 
authors:
- danny
appId: com.changelly.iosapp
appCountry: us
idd: '1435140380'
released: 2019-10-04
updated: 2025-03-04
version: 2.52.0
stars: 4.6
reviews: 5113
website: https://changelly.com
repository: 
issue: 
icon: com.changelly.iosapp.jpg
bugbounty: 
meta: ok
verdict: nowallet
appHashes: 
date: 2023-09-19
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Fintechvision Limited

---

{% include copyFromAndroid.html %}