---
wsId: ooBit
title: Oobit - Tap to Pay with Crypto
altTitle: 
authors:
- danny
appId: com.oobit
appCountry: br
idd: '1598882898'
released: 2022-02-05
updated: 2025-03-09
version: 1.8.0
stars: 2.3
reviews: 3
website: https://www.oobit.com/
repository: 
issue: 
icon: com.oobit.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Oobit Technologies

---

{% include copyFromAndroid.html %}