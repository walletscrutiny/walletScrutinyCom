---
wsId: bitbuy
title: 'Bitbuy: Buy Bitcoin Canada'
altTitle: 
authors:
- danny
appId: com.bitbuy.mobileApp
appCountry: ca
idd: '1476837869'
released: 2019-10-21
updated: 2025-03-05
version: 5.0.9
stars: 4.5
reviews: 7338
website: https://bitbuy.ca/
repository: 
issue: 
icon: com.bitbuy.mobileApp.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-09-15
signer: 
reviewArchive: 
twitter: bitbuy
social:
- https://www.linkedin.com/company/bitbuyca
- https://www.facebook.com/bitbuyCA
features: 
developerName: Bitbuy Inc

---

 {% include copyFromAndroid.html %}
