---
wsId: coinZoom
title: CoinZoom feat. ZoomMe
altTitle: 
authors:
- danny
appId: com.ios.coinzoomsimple
appCountry: us
idd: '1575983875'
released: 2022-01-21
updated: 2025-03-10
version: 3.2.13
stars: 4.7
reviews: 284
website: http://www.coinzoom.com
repository: 
issue: 
icon: com.ios.coinzoomsimple.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-03-30
signer: 
reviewArchive: 
twitter: GetCoinZoom
social:
- https://www.facebook.com/CoinZoom
- https://www.linkedin.com/company/coinzoomhq/
features: 
developerName: CoinZoom

---

{% include copyFromAndroid.html %}
