---
wsId: sFox
title: sFOX
altTitle: 
authors:
- danny
appId: com.sfox.ios
appCountry: us
idd: '1583801613'
released: 2022-04-12
updated: 2024-12-10
version: 1.14.3
stars: 4.7
reviews: 12
website: https://www.sfox.com/
repository: 
issue: 
icon: com.sfox.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2025-02-28
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: SFOX Inc

---

{% include copyFromAndroid.html %}