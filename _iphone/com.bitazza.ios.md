---
wsId: bitazza
title: 'Bitazza TH: Crypto Wallet'
altTitle: 
authors:
- danny
appId: com.bitazza.ios
appCountry: th
idd: '1476944844'
released: 2020-05-25
updated: 2025-03-05
version: 3.9.8
stars: 4.4
reviews: 1862
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-10-01
signer: 
reviewArchive: 
twitter: bitazzaofficial
social:
- https://www.linkedin.com/company/bitazza
- https://www.facebook.com/bitazza
features: 
developerName: Bitazza Company Limited

---

{% include copyFromAndroid.html %}
