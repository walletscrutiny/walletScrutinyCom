---
wsId: yuh
title: Yuh - Your App. Your Money.
altTitle: 
authors:
- danny
appId: com.swissquote.Yuh
appCountry: ch
idd: '1493935010'
released: 2021-05-10
updated: 2025-03-10
version: 1.34.4
stars: 4.7
reviews: 16505
website: https://www.yuh.com
repository: 
issue: 
icon: com.swissquote.Yuh.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
appHashes: 
date: 2023-03-02
signer: 
reviewArchive: 
twitter: yuh_app
social:
- https://www.facebook.com/yuhapp.en/
features: 
developerName: Swissquote

---

{% include copyFromAndroid.html %}
