---
wsId: secuXWalletForN
title: SecuX Wallet
altTitle: 
authors:
- danny
appId: com.secuxtech.wallet
appCountry: tw
idd: '1628469822'
released: 2022-07-04
updated: 2025-02-20
version: 3.0.0
stars: 4
reviews: 6
website: https://secux.eu/
repository: 
issue: 
icon: com.secuxtech.wallet.jpg
bugbounty: 
meta: ok
verdict: nowallet
appHashes: 
date: 2024-09-03
signer: 
reviewArchive: 
twitter: SecuXwallet
social: 
features: 
developerName: SecuX Europe GmbH

---

{% include copyFromAndroid.html %}