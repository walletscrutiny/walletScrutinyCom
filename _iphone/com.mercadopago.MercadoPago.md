---
wsId: mercadopago
title: 'Mercado Pago: cuenta digital'
altTitle: 
authors:
- leo
appId: com.mercadopago.MercadoPago
appCountry: br
idd: 925436649
released: 2014-12-17
updated: 2025-03-05
version: 2.372.1
stars: 4.8
reviews: 1888412
website: http://www.mercadopago.com
repository: 
issue: 
icon: com.mercadopago.MercadoPago.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
appHashes: 
date: 2021-12-26
signer: 
reviewArchive: 
twitter: mercadopago
social:
- https://www.facebook.com/mercadopago
features: 
developerName: MercadoLibre

---

{% include copyFromAndroid.html %}