---
wsId: mudrex
title: 'Mudrex: Buy Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.mudrex.ios
appCountry: in
idd: '1609440707'
released: 2022-03-12
updated: 2025-03-03
version: '8.49'
stars: 4.4
reviews: 3292
website: https://mudrex.com/
repository: 
issue: 
icon: com.mudrex.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-09-07
signer: 
reviewArchive: 
twitter: officialmudrex
social:
- https://www.linkedin.com/company/mudrex
- https://t.me/mudrex_community
- https://www.youtube.com/@Mudrex
- https://www.instagram.com/officialmudrex
- https://www.facebook.com/mudrex
features: 
developerName: Mudrex

---

{% include copyFromAndroid.html %}