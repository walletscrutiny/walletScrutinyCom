---
wsId: ownbit
title: 'Ownbit: Cold & MultiSig Wallet'
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: '1321798216'
released: 2018-02-07
updated: 2025-03-07
version: 4.56.0
stars: 4.2
reviews: 53
website: http://www.bitbill.com
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BITBILL PTY LTD

---

{% include copyFromAndroid.html %}
