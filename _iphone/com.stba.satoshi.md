---
wsId: 
title: Satoshi | The Bitcoin App
altTitle: 
authors: 
appId: com.stba.satoshi
appCountry: us
idd: '6463799763'
released: 2024-11-19
updated: 2025-03-03
version: 1.0.11
stars: 4.2
reviews: 24
website: https://satoshi.money/
repository: 
issue: 
icon: com.stba.satoshi.jpg
bugbounty: 
meta: ok
verdict: wip
appHashes: 
date: 2024-12-17
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Satoshi

---

