---
wsId: getDelta
title: Delta Investment Tracker
altTitle: 
authors: 
appId: io.getdelta.ios
appCountry: us
idd: 1288676542
released: 2017-09-25
updated: 2025-03-10
version: 2025.1.2
stars: 4.7
reviews: 11281
website: https://delta.app
repository: 
issue: 
icon: io.getdelta.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
appHashes: 
date: 2021-11-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Opus Labs CVBA

---

{% include copyFromAndroid.html %}
