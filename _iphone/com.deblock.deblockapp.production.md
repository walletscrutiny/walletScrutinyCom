---
wsId: deblock
title: Deblock - Current Account
altTitle: 
authors:
- danny
appId: com.deblock.deblockapp.production
appCountry: fr
idd: '6479202981'
released: 2024-04-02
updated: 2025-03-10
version: 3.0.0
stars: 4.7
reviews: 5033
website: https://deblock.com/en-FR
repository: 
issue: 
icon: com.deblock.deblockapp.production.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-09-06
signer: 
reviewArchive: 
twitter: DeblockApp
social:
- https://discord.com/invite/deblock
features: 
developerName: Deblock

---

{% include copyFromAndroid.html %}