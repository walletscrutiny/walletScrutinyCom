---
wsId: unbankBitcoin
title: 'Unbank: Buy & Sell Bitcoin'
altTitle: 
authors:
- danny
appId: world.unbank.unbankmobileapp
appCountry: us
idd: '1587374229'
released: 2022-05-03
updated: 2025-03-07
version: 2.10.4
stars: 4
reviews: 39
website: https://www.unbank.com/
repository: 
issue: 
icon: world.unbank.unbankmobileapp.jpg
bugbounty: 
meta: ok
verdict: nowallet
appHashes: 
date: 2023-08-30
signer: 
reviewArchive: 
twitter: unbankworld
social:
- https://www.facebook.com/unbankworld
- https://www.instagram.com/unbankworld
features: 
developerName: KALBAS INC

---

{% include copyFromAndroid.html %}
