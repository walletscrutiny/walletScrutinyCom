---
wsId: Moneybrain
title: Moneybrain Financial SuperApp
altTitle: 
authors:
- danny
appId: com.moneybrain.moneybrain
appCountry: gb
idd: 1476827262
released: 2019-10-15
updated: 2025-02-23
version: 3.5.6
stars: 4.5
reviews: 10
website: https://www.moneybrain.com
repository: 
issue: 
icon: com.moneybrain.moneybrain.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-11-22
signer: 
reviewArchive: 
twitter: MoneybrainBiPS
social: 
features: 
developerName: Moneybrain LTD

---

{% include copyFromAndroid.html %}