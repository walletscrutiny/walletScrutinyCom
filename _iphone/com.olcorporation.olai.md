---
wsId: iMeMessenger
title: 'iMe: AI Messenger for Telegram'
altTitle: 
authors:
- danny
appId: com.olcorporation.olai
appCountry: us
idd: '1450480822'
released: 2019-07-31
updated: 2025-02-23
version: 11.7.2
stars: 4.7
reviews: 6875
website: https://imem.app/
repository: 
issue: 
icon: com.olcorporation.olai.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2024-09-07
signer: 
reviewArchive: 
twitter: ImePlatform
social:
- https://t.me/ime_en
- https://imesmartplatform.medium.com
- https://www.youtube.com/c/iMeMessenger
- https://www.reddit.com/r/iMeSmartPlatform
features: 
developerName: iMe Lab LTD

---

{% include copyFromAndroid.html %}
