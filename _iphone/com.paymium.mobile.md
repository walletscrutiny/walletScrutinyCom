---
wsId: Paymium
title: Paymium
altTitle: 
authors:
- danny
appId: com.paymium.mobile
appCountry: fr
idd: 1055288395
released: 2016-01-18
updated: 2025-02-21
version: 8.12.5
stars: 3.1
reviews: 21
website: https://www.paymium.com/
repository: 
issue: 
icon: com.paymium.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2024-10-07
signer: 
reviewArchive: 
twitter: paymium
social:
- https://www.linkedin.com/company/paymium
- https://www.facebook.com/Paymium
features: 
developerName: Paymium SAS

---

{% include copyFromAndroid.html %}
