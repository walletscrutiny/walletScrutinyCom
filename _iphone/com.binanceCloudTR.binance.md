---
wsId: BinanceTR
title: 'Binance TR: Bitcoin and Crypto'
altTitle: 
authors:
- danny
appId: com.binanceCloudTR.binance
appCountry: tr
idd: '1548636153'
released: 2021-02-18
updated: 2025-02-23
version: 2.14.0
stars: 4.7
reviews: 138503
website: https://www.trbinance.com/
repository: 
issue: 
icon: com.binanceCloudTR.binance.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2021-10-21
signer: 
reviewArchive: 
twitter: BinanceTR
social:
- https://www.facebook.com/TRBinanceTR
features: 
developerName: BN TEKNOLOJİ ANONİM ŞİRKETİ

---

{% include copyFromAndroid.html %}
