---
wsId: letsBit
title: 'LB Finanzas: billetera virtual'
altTitle: 
authors:
- danny
appId: com.letsbit.app
appCountry: ar
idd: '1644159531'
released: 2022-12-07
updated: 2025-02-17
version: 1.47.0
stars: 4.4
reviews: 362
website: https://www.lbfinanzas.com
repository: 
issue: 
icon: com.letsbit.app.jpg
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-04-07
signer: 
reviewArchive: 
twitter: LetsBit_ok
social:
- https://www.youtube.com/channel/UCTxHaohwsq9x9mhqW7XBnzw
- https://www.linkedin.com/company/letsbit/
- https://www.facebook.com/LetsBit
- https://www.instagram.com/letsbit/
features: 
developerName: Let'sBit

---

{% include copyFromAndroid.html %}

