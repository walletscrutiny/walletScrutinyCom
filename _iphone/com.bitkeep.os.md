---
wsId: bitkeep
title: 'Bitget Wallet: Crypto, Trump'
altTitle: 
authors:
- leo
appId: com.bitkeep.os
appCountry: 
idd: 1395301115
released: 2018-09-26
updated: 2025-02-25
version: 8.29.1
stars: 4.7
reviews: 4557
website: https://web3.bitget.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
meta: ok
verdict: nosource
appHashes: 
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitKeepOS
social:
- https://www.facebook.com/bitkeep
- https://github.com/bitkeepcom
features: 
developerName: BitKeep Global Inc.

---

 {% include copyFromAndroid.html %}
