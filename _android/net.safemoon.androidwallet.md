---
wsId: safemoon
title: VGX Wallet
altTitle: 
authors:
- danny
users: 100000
appId: net.safemoon.androidwallet
appCountry: 
released: 2021-09-10
updated: 2025-03-06
version: V4.1
stars: 4.1
ratings: 
reviews: 6369
website: https://vgxfoundation.com
repository: 
issue: 
icon: net.safemoon.androidwallet.png
bugbounty: 
meta: ok
verdict: nobtc
appHashes: 
date: 2025-02-26
signer: 
reviewArchive: 
twitter: safemoon
social: 
redirect_from: 
developerName: VGX Foundation
features: 

---

## App Description 

The Safemoon wallet supports assets from the following: 

- ERC20 (Ethereum)
- BEP20 (Binance)
- Polygon
- Avalanche
- Solana

## Analysis 

This wallet does not have Bitcoin support. 
