---
wsId: ncWallet
title: 'NC Wallet: Crypto Without Fees'
altTitle: 
authors:
- danny
users: 1000000
appId: com.ncwallet
appCountry: 
released: 2022-10-18
updated: 2025-02-26
version: 1.0.728
stars: 4.3
ratings: 
reviews: 481
website: https://ncwallet.net
repository: 
issue: 
icon: com.ncwallet.png
bugbounty: 
meta: ok
verdict: custodial
appHashes: 
date: 2023-06-14
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: NCW Team
features: 

---

## App Description from Google Play 

> Send, store, exchange, withdraw and make purchases in all major cryptocurrencies with no limits...

## Analysis 

- We tested the app, but was not able to find any mnemonic phrases. 
- There was a BTC wallet that can send and receive.
- The app's [Terms](https://ncwallet.net/en/terms/) defines storage services and fees, for storing "dormant" cryptocurrencies with no transactions for more than 12 months.
- The terms also give the platform the right to "suspend the user's account" and "block all Cryptocurrencies" in that account. 
- This is clearly a **custodial** provider.